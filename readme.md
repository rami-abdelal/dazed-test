#Dazed Media Technical Test

##Clock

This is a fizz-buzz cuckoo assignment. Simply run npm install to get Babel up for ES6 transpilation and open clock.html. The code is in clock.js. To use the app, enter a
24 hour time such as 00:46 in the input element at the top of the page and hit enter
to see the result.

##Carousel

Using the Owl Carousel jQuery plugin, this page displays 6 elements that can be
navigated with drag, swipe, next and previous arrows, and tracking dots. At
resolutions above 600px wide, 2 elements are in view. Otherwise, only one element
is visible. Just open up carousel.html to use it.
