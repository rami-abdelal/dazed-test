/**
 * Inserts a message into the DOM. If input is supplied,
 * it will insert your input first, then the response.
 *
 * @param {string} message  The message to insert
 * @param {string} input    Optional, the original input
 */
function setMessage(message, input) {
  /**
   * Specify element details
   */
  const displayContainer = document.getElementById("display");
  const displayElement = document.createElement("h1");
  const className = input ? "message--outbound" : "message--inbound";
  const text = input ? input : message;
  const node = document.createTextNode(text);
  /**
   * Insert element
   */
  displayElement.classList.add("message", className);
  displayElement.appendChild(node);
  displayContainer.prepend(displayElement);
  /**
   * Repeat once for response
   */
  if (input) {
    setTimeout(setMessage, 1000, message);
  }
}

/**
 * Returns a message depending on the time provided
 *
 * @param {string|number} hours
 * @param {string|number} minutes
 * @return {string} Message to be inserted into the DOM
 */
function getMessage(hours, minutes) {
  let message = "";

  const minutesAre = {
    divisibleBy: denominator => {
      return minutes % denominator === 0;
    }
  };

  /**
   * Divisible by 3 and 5
   */
  if (minutesAre.divisibleBy(3) && minutesAre.divisibleBy(5)) {
    /**
     * Whole hour
     */
    if (minutes == 0) {
      if (hours == 0) hours = 12;
      else if (hours > 12) hours -= 12;
      for (let i = 1; i <= hours; i++) {
        message += i == hours ? "Cuckoo" : "Cuckoo ";
      }
      /**
       * Half hour
       */
    } else if (minutes == 30) {
      message = "Cuckoo";
      /**
       * Divisible by 3 and 5
       */
    } else {
      message = "Fizz Buzz";
    }
    /**
     * Divisible by 3
     */
  } else if (minutesAre.divisibleBy(3)) {
    message = "Fizz";
    /**
     * Divisible by 5
     */
  } else if (minutesAre.divisibleBy(5)) {
    message = "Buzz";
    /**
     * Default
     */
  } else {
    message = "tick";
  }
  return message;
}

/**
 * Validates input, gets, and sets appropriate message
 *
 * @param {string|number} input
 */
function submitHandler(input) {
  /**
   * Show input and response if valid time is supplied
   */
  if (input.match(/^(0[0-9]|1[0-9]|2[0-3]):[0-5][0-9]$/)) {
    const [hours, minutes] = input.split(":");
    const message = getMessage(hours, minutes);
    setMessage(`${input}: ${message}`, input);
    /**
     * Or show error
     */
  } else
    setMessage(
      document.querySelector(".form__input").getAttribute("placeholder"),
      input
    );
}

/**
 * Initialise app by binding submitHandler
 */
(function init() {
  const inputElement = document.querySelector(".form__input");
  const formElement = document.querySelector("form.form");
  formElement.addEventListener("submit", e => {
    e.preventDefault();
    submitHandler(inputElement.value);
    inputElement.value = "";
  });
})();
