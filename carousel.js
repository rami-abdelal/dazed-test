$(document).ready(function() {
  $(".owl-carousel").owlCarousel({
    loop: true,
    responsiveClass: true,
    nav: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 2
      }
    }
  });
});
